package com.laksh;

import java.io.File;
import java.util.List;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.ExecCreateCmdResponse;
import com.github.dockerjava.api.command.InspectExecResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.command.BuildImageResultCallback;

/**
 * Hello world!
 *
 */
public class App {

    static DockerClient dockerClient = DockerClientBuilder.getInstance().build();

    public static void main(String[] args) {
        System.out.println("Hello Docker!");
        // listImages();
        String imageId = createImage();
        // listImages();
        CreateContainerResponse container = createContainer(imageId);
        startContainer(container);
        System.out.println("We're done.");

    }

    private static void listImages() {
        System.out.println("Listing Images...");
        List<Image> images = dockerClient.listImagesCmd().exec();
        images.stream().forEach(i -> System.out.println(i.getId()));
        System.out.println("...");
    }

    private static void listContainers() {
        System.out.println("Listing Containers...");
        List<Container> containers = dockerClient.listContainersCmd().withShowSize(true).withShowAll(true).exec();
        containers.stream().forEach(c -> System.out.println(c.getId()));
        System.out.println("...");
    }

    private static String createImage() {
        String dockerfile = "/Users/ls043274/MySpace/Docker/docker-with-java/Dockerfile";
        String imageId = dockerClient.buildImageCmd().withDockerfile(new File(dockerfile)).withPull(true)
                .withNoCache(true).exec(new BuildImageResultCallback()).awaitImageId();
        System.out.println("Created Image:" + imageId);
        return imageId;
    }

    private static CreateContainerResponse createContainer(String imageId) {
        String path = "input-function.py";
        CreateContainerResponse containerResponse = dockerClient.createContainerCmd(imageId).withEnv("FUNCTION=" + path)
                .withCmd("python app.py").exec();
        System.out.println("Created Container:" + containerResponse.getId());
        return containerResponse;
    }

    private static void startContainer(CreateContainerResponse container) {
        System.out.println("Starting container - " + container.getId());
        dockerClient.startContainerCmd(container.getId()).exec();
        ExecCreateCmdResponse cmdResponse = dockerClient.execCreateCmd(container.getId()).withCmd("python", "app.py")
                .exec();
        InspectExecResponse inspectResponse = dockerClient.inspectExecCmd(cmdResponse.getId()).exec();
        System.out.println(inspectResponse.getExitCode());
        System.out.println(inspectResponse);
    }
}
