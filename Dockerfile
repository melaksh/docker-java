FROM python:2.7-slim
WORKDIR /app
COPY src/main/python /app
RUN chmod 777 input-function.py
# ENV FUNCTION input-function.py
ENTRYPOINT ["tail", "-f", "/dev/null"]
CMD ["python", "app.py"]

